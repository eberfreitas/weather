# Weather API

API simples para verificação de dados climáticos de cidades.

## Rodando a aplicação

Para executar a aplicação e acessar a API basta clonar o repositório e na raiz
do projeto rodar o seguinte comando:

```
$ mix phx.server
```

## Rodando testes

Para executar os testes basta executar este comando:

```
$ mix test
```

## Acessando os dados via API

Com o servidor rodando você agora pode acessar os dados da API.

### GET /api/cities

Neste endpoint você pode acessar todas as cidades presentes no sistema.

**Exemplo**

```
$ curl http://localhost:4000/api/cities
```

#### Filtros

- `weather=true`

Passando o parâmetro `weather` como `true` você pode limitar o resultado das
cidades apenas dentre aquelas que possuem dados de clima.

**Exemplo**

```
$ curl http://localhost:4000/api/cities?weather=true
```

- `latlon={coordenadas}`

Filtra as cidades por dados de coordenadas. Os valores da latitude e longitude
devem ser enviados separados por vírgula.

**Exemplo**

```
$ curl http://localhost:4000/api/cities?latlon=19.85,-90.533333
```

### GET /api/cities/{id}

Para acessar os dados de uma cidade específica basta passar o **id** como
parâmetro neste endpoint.

**Exemplo**

```
$ curl http://localhost:4000/api/cities/3531732
```

#### Filtros

- `from={AAAA-MM-DD}&to={AAAA-MM-DD}`

Nos casos onde uma cidade possui diversos dados climáticos é possível restringir
os resultados destes dados por data usando os parâmetros `from` e `to`.

**Exemplo**

```
$ curl http://localhost:4000/api/cities/3531732?from=2017-03-13&to=2017-03-15
```
