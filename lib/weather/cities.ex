defmodule Weather.Cities do
  @moduledoc """
  The Cities context.
  """

  alias Weather.Cities.City

  @city_list "priv/city_list.json"
  @weather_list "priv/weather_list.json"

  @doc """
  Returns the list of cities.
  """
  @spec list_cities() :: :error | {:ok, [City.t(), ...]}
  def list_cities do
    with {:ok, raw_city_list} <- File.read(@city_list),
         {:ok, raw_weather_list} <- File.read(@weather_list),
         {:ok, city_list} <- Jason.decode(raw_city_list),
         {:ok, weather_list} <- Jason.decode(raw_weather_list) do
      data =
        Enum.map(city_list, fn i ->
          weather = find_weather(i["id"], weather_list)
          Map.put(i, "weather", weather) |> City.new()
        end)

      {:ok, data}
    else
      _ -> :error
    end
  end

  @doc """
  Filter cities when they have weather data.
  """
  @spec filter_by_weather_data([City.t(), ...]) :: [] | [City.t(), ...]
  def filter_by_weather_data(cities) do
    Enum.filter(cities, fn c -> length(c.weather) > 0 end)
  end

  @doc """
  Filter cities by latitude/longitude.
  """
  @spec filter_by_coordinates([City.t(), ...], float(), float()) :: [] | [City.t(), ...]
  def filter_by_coordinates(cities, lat, lon) do
    Enum.filter(cities, fn c ->
      c.coord["lat"] == lat && c.coord["lon"] == lon
    end)
  end

  @doc """
  Gets a single city.
  """
  @spec get_city(integer()) :: {:ok, City.t()} | :error
  def get_city(id) do
    {:ok, cities} = list_cities()

    case Enum.find(cities, :error, fn i -> i.id == id end) do
      :error -> :error
      city -> {:ok, city}
    end
  end

  @doc """
  Filters weather data in a city record by a date range.
  """
  @spec filter_city_weather(City.t(), binary | DateTime.t(), binary | DateTime.t()) :: City.t()
  def filter_city_weather(city, %DateTime{} = from, %DateTime{} = to) do
    range = DateTime.to_unix(from)..DateTime.to_unix(to)

    weather =
      city.weather
      |> Enum.filter(fn w ->
        w.dt in range
      end)

    Map.put(city, :weather, weather)
  end

  def filter_city_weather(city, from, to) do
    {:ok, from} = DateTimeParser.parse_datetime(from <> " 00:00:00", assume_utc: true)
    {:ok, to} = DateTimeParser.parse_datetime(to <> " 23:59:59", assume_utc: true)

    filter_city_weather(city, from, to)
  end

  @spec find_weather(integer(), [...]) :: [] | [...]
  defp find_weather(city_id, weather_list) do
    weather_list
    |> Enum.find(%{}, fn w -> w["cityId"] == city_id end)
    |> Map.get("data", [])
  end
end
