defmodule Weather.Cities.City do
  @moduledoc """
  Ecto schema to represent one city record.
  """

  use Ecto.Schema

  import Ecto.Changeset

  alias Weather.Cities.Weather

  @typedoc """
  Represents one city record.
  """
  @type t :: %__MODULE__{
          id: integer(),
          coord: map(),
          country: String.t(),
          name: String.t(),
          stat: map(),
          zoom: integer(),
          geoname: struct(),
          stations: [struct(), ...],
          weather: [] | [Weather.t(), ...]
        }

  @primary_key {:id, :integer, autogenerate: false}

  @derive Jason.Encoder
  embedded_schema do
    field(:coord, {:map, :float})
    field(:country, :string)
    field(:name, :string)
    field(:stat, {:map, :float})
    field(:zoom, :integer)

    embeds_one :geoname, GeoName, primary_key: false do
      field(:cl, :string)
      field(:code, :string)
      field(:parent, :integer)
    end

    embeds_many :stations, Stations, primary_key: {:id, :integer, autogenerate: false} do
      field(:dist, :integer)
      field(:kf, :integer)
    end

    embeds_many(:weather, Weather)
  end

  @doc """
  Creates a new City struct with the given params.
  """
  @spec new(map()) :: __MODULE__.t()
  def new(params) do
    %__MODULE__{}
    |> changeset(params)
    |> apply_changes()
  end

  @doc """
  Produces the changeset.
  """
  @spec changeset(struct(), map()) :: Ecto.Changeset.t()
  def changeset(schema, params) do
    schema
    |> cast(params, [:id, :coord, :country, :name, :stat, :zoom])
    |> cast_embed(:geoname, with: &geoname_changeset/2)
    |> cast_embed(:stations, with: &station_changeset/2)
    |> cast_embed(:weather, with: &Weather.changeset/2)
  end

  @spec geoname_changeset(struct(), map()) :: Ecto.Changeset.t()
  defp geoname_changeset(schema, params) do
    schema
    |> cast(params, [:cl, :code, :parent])
  end

  @spec station_changeset(struct(), map()) :: Ecto.Changeset.t()
  defp station_changeset(schema, params) do
    schema
    |> cast(params, [:id, :dist, :kf])
  end
end

require Protocol

Protocol.derive(Jason.Encoder, Weather.Cities.City.GeoName)
Protocol.derive(Jason.Encoder, Weather.Cities.City.Stations)
