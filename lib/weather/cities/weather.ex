defmodule Weather.Cities.Weather do
  @moduledoc """
  Ecto schema for the weather data portion of a city record.
  """

  use Ecto.Schema

  import Ecto.Changeset

  @typedoc """
  Represents the weather data portion of a city record.
  """
  @type t :: %__MODULE__{
          dt: integer(),
          temp: map(),
          pressure: float(),
          humidity: float(),
          speed: float(),
          deg: integer(),
          clouds: integer(),
          uvi: float(),
          weather: [struct(), ...] | []
        }

  @primary_key false

  @derive Jason.Encoder
  embedded_schema do
    field(:dt, :integer)
    field(:temp, {:map, :float})
    field(:pressure, :float)
    field(:humidity, :float)
    field(:speed, :float)
    field(:deg, :integer)
    field(:clouds, :integer)
    field(:uvi, :float)

    embeds_many :weather, Details, primary_key: {:id, :integer, autogenerate: false} do
      field(:main, :string)
      field(:description, :string)
      field(:icon, :string)
    end
  end

  @doc """
  Produces the changeset.
  """
  @spec changeset(struct(), map()) :: Ecto.Changeset.t()
  def changeset(schema, params) do
    schema
    |> cast(params, [:dt, :temp, :pressure, :humidity, :speed, :deg, :clouds, :uvi])
    |> cast_embed(:weather, with: &details_changelog/2)
  end

  @spec details_changelog(struct(), map()) :: Ecto.Changeset.t()
  defp details_changelog(schema, params) do
    schema
    |> cast(params, [:id, :main, :description, :icon])
  end
end

require Protocol

Protocol.derive(Jason.Encoder, Weather.Cities.Weather.Details)
