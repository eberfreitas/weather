defmodule WeatherWeb.CityView do
  use WeatherWeb, :view

  def render("index.json", %{cities: cities}) do
    cities
  end

  def render("show.json", %{city: city}) do
    city
  end
end
