defmodule WeatherWeb.CityController do
  use WeatherWeb, :controller

  alias Weather.Cities
  alias Weather.Cities.City

  action_fallback WeatherWeb.FallbackController

  @spec index(Plug.Conn.t(), map()) :: :error | Plug.Conn.t()
  def index(conn, params) do
    with {:ok, cities} <- Cities.list_cities(),
         {:ok, cities} <- filter_by_weather(params, cities),
         {:ok, cities} <- filter_by_coordinates(params, cities) do
      render(conn, "index.json", cities: cities)
    end
  end

  @spec filter_by_coordinates(map(), [City.t(), ...]) :: {:ok, [City.t(), ...]}
  defp filter_by_coordinates(%{"latlon" => latlon}, cities) do
    [lat, lon] = String.split(latlon, ",")
    {:ok, Cities.filter_by_coordinates(cities, String.to_float(lat), String.to_float(lon))}
  end

  defp filter_by_coordinates(_params, cities), do: {:ok, cities}

  @spec filter_by_weather(map(), [City.t(), ...]) :: {:ok, [City.t(), ...]}
  defp filter_by_weather(%{"weather" => "true"}, cities) do
    {:ok, Cities.filter_by_weather_data(cities)}
  end

  defp filter_by_weather(_params, cities), do: {:ok, cities}

  @spec show(Plug.Conn.t(), map()) :: :error | Plug.Conn.t()
  def show(conn, %{"id" => id} = params) do
    id = String.to_integer(id)

    with {:ok, city} <- Cities.get_city(id),
         {:ok, city} <- filter_weather_by_date(params, city) do
      render(conn, "show.json", city: city)
    end
  end

  @spec filter_weather_by_date(map(), City.t()) :: {:ok, City.t()}
  defp filter_weather_by_date(%{"from" => from, "to" => to}, city) do
    {:ok, Cities.filter_city_weather(city, from, to)}
  end

  defp filter_weather_by_date(_params, city), do: {:ok, city}
end
