defmodule WeatherWeb.FallbackController do
  @moduledoc """
  Translates controller action results into valid `Plug.Conn` responses.

  See `Phoenix.Controller.action_fallback/1` for more details.
  """
  use WeatherWeb, :controller

  def call(conn, {:error, :not_found}) do
    conn
    |> put_status(:not_found)
    |> put_view(WeatherWeb.ErrorView)
    |> render(:"404")
  end

  def call(conn, :error) do
    call(conn, {:error, :not_found})
  end
end
