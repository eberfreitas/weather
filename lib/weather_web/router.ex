defmodule WeatherWeb.Router do
  use WeatherWeb, :router

  pipeline :api do
    plug :accepts, ["json"]
  end

  scope "/api", WeatherWeb do
    pipe_through :api

    resources "/cities", CityController, only: [:index, :show]
  end
end
