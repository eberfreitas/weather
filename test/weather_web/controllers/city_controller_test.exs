defmodule WeatherWeb.CityControllerTest do
  use WeatherWeb.ConnCase

  setup %{conn: conn} do
    {:ok, conn: put_req_header(conn, "accept", "application/json")}
  end

  describe "index" do
    test "lists all cities", %{conn: conn} do
      conn = get(conn, Routes.city_path(conn, :index))
      assert json_response(conn, 200) != []
    end

    test "list all cities with weather info", %{conn: conn} do
      conn = get(conn, Routes.city_path(conn, :index, %{"weather" => "true"}))
      resp = json_response(conn, 200)
      assert length(resp) == 2
    end

    test "list city by coordinates", %{conn: conn} do
      conn = get(conn, Routes.city_path(conn, :index, %{"latlon" => "19.85,-90.533333"}))
      resp = json_response(conn, 200)
      assert length(resp) == 1
    end
  end

  describe "show" do
    test "a city", %{conn: conn} do
      conn = get(conn, Routes.city_path(conn, :show, 3_862_351))
      resp = json_response(conn, 200)
      assert resp["id"] == 3_862_351
    end

    test "404", %{conn: conn} do
      conn = get(conn, Routes.city_path(conn, :show, 0))
      resp = json_response(conn, 404)
      assert resp["errors"]["detail"] == "Not Found"
    end

    test "a city with filtered weather data", %{conn: conn} do
      conn =
        get(
          conn,
          Routes.city_path(conn, :show, 3_531_732, %{"from" => "2017-03-13", "to" => "2017-03-15"})
        )

      resp = json_response(conn, 200)
      assert length(resp["weather"]) == 3
    end
  end
end
