defmodule Weather.Cities.WeatherTest do
  use ExUnit.Case

  alias Weather.Cities.Weather

  test "changeset/2" do
    sample_data = """
    {
      "dt":1489428000,
      "temp":{
         "day":299.15,
         "min":298.2,
         "max":299.15,
         "night":298.2,
         "eve":299.15,
         "morn":299.15
      },
      "pressure":1027.35,
      "humidity":100,
      "weather":[
        {
          "id":803,
          "main":"Clouds",
          "description":"broken clouds",
          "icon":"04n"
        }
      ],
      "speed":6.71,
      "deg":337,
      "clouds":68,
      "uvi":10.34,
      "bogus": "yadayada"
    }
    """

    {:ok, params} = Jason.decode(sample_data)
    changeset = Weather.changeset(%Weather{}, params)
    weather = Ecto.Changeset.apply_changes(changeset)

    assert changeset.valid?
    assert weather.dt == 1_489_428_000
    assert weather.temp["day"] == 299.15
    assert :error = Map.fetch(weather, :bogus)

    [detail | _] = weather.weather

    assert detail.id == 803
  end
end
