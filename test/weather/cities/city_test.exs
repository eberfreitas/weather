defmodule Weather.Cities.CityTest do
  use ExUnit.Case

  alias Weather.Cities.City

  @sample_data """
  {
    "id": 3531732,
    "coord": {
      "lon": -90.533333,
      "lat": 19.85
    },
    "country": "MX",
    "geoname": {
      "cl": "P",
      "code": "PPLA",
      "parent": 6942858
    },
    "name": "Campeche",
    "stat": {
      "level": 1.0,
      "population": 205212
    },
    "stations": [
      {
        "id": 3968,
        "dist": 4,
        "kf": 1
      }
    ],
    "zoom": 7,
    "weather": [
      {
        "dt":1489428000,
        "temp":{
           "day":299.15,
           "min":298.2,
           "max":299.15,
           "night":298.2,
           "eve":299.15,
           "morn":299.15
        },
        "pressure":1027.35,
        "humidity":100,
        "weather":[
          {
            "id":803,
            "main":"Clouds",
            "description":"broken clouds",
            "icon":"04n"
          }
        ],
        "speed":6.71,
        "deg":337,
        "clouds":68,
        "uvi":10.34,
        "bogus": "yadayada"
      }
    ]
  }
  """

  test "changeset/2" do
    changeset = City.changeset(%City{}, params())

    assert changeset.valid?
  end

  test "new/1" do
    city = City.new(params())
    [station | _] = city.stations
    [weather | _] = city.weather

    assert city.id == 3_531_732
    assert city.coord["lon"] == -90.533333
    assert city.geoname.cl == "P"
    assert city.stat["level"] == 1.0
    assert station.id == 3968
    assert weather.dt == 1_489_428_000
  end

  defp params do
    {:ok, params} = Jason.decode(@sample_data)
    params
  end
end
