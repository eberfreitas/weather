defmodule Weather.CitiesTest do
  use ExUnit.Case

  alias Weather.Cities
  alias Weather.Cities.City

  test "list_cities/0 returns all cities" do
    assert {:ok, cities} = Cities.list_cities()
  end

  describe "get_city/1" do
    test "returns one city" do
      id = 3_531_732
      assert {:ok, %City{id: ^id}} = Cities.get_city(id)
    end

    test "returns :error" do
      assert :error = Cities.get_city(0)
    end
  end

  test "filter_by_weather_data/1" do
    {:ok, all_cities} = Cities.list_cities()
    weather_cities = Cities.filter_by_weather_data(all_cities)
    [city | _] = weather_cities

    assert length(all_cities) > length(weather_cities)
    assert length(weather_cities) > 0
    assert length(city.weather) > 0
  end

  describe "filter_by_coordinates/1" do
    setup do
      {:ok, all_cities} = Cities.list_cities()
      [cities: all_cities]
    end

    test "finds a city", %{cities: cities} do
      coordinate_city = Cities.filter_by_coordinates(cities, -33.04417, -61.16806)
      [city | _] = coordinate_city
      assert city.id == 3_862_351
    end

    test "finds no city", %{cities: cities} do
      empty_city_list = Cities.filter_by_coordinates(cities, 0, 0)
      assert Enum.empty?(empty_city_list)
    end
  end

  describe "filter_city_weather/3" do
    setup do
      {:ok, city} = Cities.get_city(3_531_732)
      [city: city]
    end

    test "filters weather", %{city: city} do
      city = Cities.filter_city_weather(city, "2017-03-13", "2017-03-15")
      assert length(city.weather) == 3
    end

    test "filters out all weather", %{city: city} do
      city = Cities.filter_city_weather(city, "2016-03-13", "2016-03-15")
      assert Enum.empty?(city.weather)
    end
  end
end
